/**
 * Extend the basic ActorSheet with some very simple modifications
 * @extends {ActorSheet}
 */
export class origoActorSheet extends ActorSheet {

  /** @override */
  static get defaultOptions() {
    return mergeObject(super.defaultOptions, {
      classes: ["origo", "sheet", "actor"],
      template: "systems/origo/templates/actor/actor-sheet.html",
      width: 1000,
      height: 1000,
      tabs: [{ navSelector: ".sheet-tabs", contentSelector: ".sheet-body", initial: "personnage" }]
    });
  }

  /* -------------------------------------------- */

 

  /** @override */
  getData() {
    const data = super.getData();
    const actorData = data.actor;

    // Prepare items.
    if (actorData.type === 'joueur' || actorData.type === 'monstre' || actorData.type === 'pnj') {
      this._prepareCharacterItems(data);
      this._prepareCharacterMacro(data);
    }
    return data;
  }

  /** @override */
  activateListeners(html) {
    super.activateListeners(html);

    // Everything below here is only needed if the sheet is editable
    if (!this.options.editable) return;

    // Drag events for macros.
    let handler = ev => this._onDragStart(ev);
    // Find all items on the character sheet.
    html.find('li.item').each((i, li) => {
      // Ignore for the header row.
      if (li.classList.contains("item-header")) return;
      // Add draggable attribute and dragstart listener.
      li.setAttribute("draggable", true);
      li.addEventListener("dragstart", handler, false);
    });

    // Add Inventory Item
    html.find('.item-create').click(this._onItemCreate.bind(this));

    // Update Inventory Item
    html.find('.item-edit').click(ev => {
      const li = $(ev.currentTarget).parents(".item");
      const item = this.actor.data.items.get(li.data("itemId"));
      item.sheet.render(true);
    });

    // Update Inventory Item
    html.find('.constitution').click(this._onRoll.bind(this));
    html.find('.force').click(this._onRoll.bind(this));
    html.find('.adresse').click(this._onRoll.bind(this));
    html.find('.sagesse').click(this._onRoll.bind(this));
    html.find('.savoir').click(this._onRoll.bind(this));
    html.find('.charisme').click(this._onRoll.bind(this));
    html.find('.perception').click(this._onRoll.bind(this));

    // Delete Inventory Item
    html.find('.item-delete').click(ev => {
      const li = $(ev.currentTarget).parents(".item");
      this.deleteEmbeddedDocuments("Item", li.data("itemId"));
      li.slideUp(200, () => this.render(false));
    });

    // Rollable abilities.
    html.find('.rollable').click(this._onRoll.bind(this));
  }

  /** @override */
  _onDragStart(event) {
    console.log(event);
  }

  /* -------------------------------------------- */

  /**
   * Handle creating a new Owned Item for the actor using initial data defined in the HTML dataset
   * @param {Event} event   The originating click event
   * @private
   */
  _onItemCreate(event) {
    event.preventDefault();
    const header = event.currentTarget;
    // Get the type of item to create.
    const type = header.dataset.type;
    // Grab any data associated with this control.
    const data = duplicate(header.dataset);
    // Initialize a default name.
    const name = `New ${type.capitalize()}`;
    // Prepare the item object.
    const itemData = {
      name: name,
      type: type,
      data: data
    };
    // Remove the type from the dataset since it's in the itemData.type prop.
    delete itemData.data["type"];

    // Finally, create the item!
    return this.createEmbeddedDocuments("Item", itemData);
  }

  _prepareCharacterItems(sheetData) {
    const actorData = sheetData.actor;
  
    // Initialize containers.
    const objet = [];
    const armure = [];
    const arme = [];
    const grande_arme = [];
  
    // Iterate through items, allocating to containers
    // let totalWeight = 0;
    for (let i of sheetData.items) {
      i.img = i.img || DEFAULT_TOKEN;
      if (i.type === 'objet') {
        objet.push(i);
      }
      else if (i.type === 'armure') {
        armure.push(i);
      }
      else if (i.type === 'arme à une main') {
        arme.push(i);
      }
      else if (i.type === 'arme à deux mains') {
        grande_arme.push(i);
      }
    }
  
    // Assign and return
    actorData.objet = objet;
    actorData.armure = armure;
    actorData.arme = arme;
    actorData.grande_arme = grande_arme;
  }

  _prepareCharacterMacro(sheetData) {
    const actorData = sheetData.actor;
    actorData.commande = game.macros;
  }

    /**
   * Handle clickable rolls.
   * @param {Event} event   The originating click event
   * @private
   */
  _onRoll(event) {
    // TODO
    // let value = event.currentTarget.dataset.value;
    // let name = event.currentTarget.dataset.name;
    // console.log("_onRoll");
    // console.log(event);
    // console.log(value);
    // let roll = new Roll("1d20 + " + value);

    // roll.toMessage({
    //   speaker: ChatMessage.getSpeaker({ actor: this.actor }),
    //   flavor: name
    // });
  }
}
