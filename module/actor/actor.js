/**
 * Extend the base Actor entity by defining a custom roll data structure which is ideal for the Simple system.
 * @extends {Actor}
 */
export class origoActor extends Actor {

  /**
   * Augment the basic actor data with additional dynamic data.
   */
  prepareData() {
    super.prepareData();

    const actorData = this.data;
    const data = actorData.data;
    const flags = actorData.flags;

    // Make separate methods for each Actor type (character, npc, etc.) to keep
    // things organized.
    if (actorData.type === 'joueur' || actorData.type === 'monstre' || actorData.type === 'pnj') { 
      this._prepareCharacterData(actorData);
    }

  }

  /**
   * Prepare Character type specific data
   */
  _prepareCharacterData(actorData) {
    const data = actorData.data;
    const actor = actorData.name;
    const items = actorData.items;
    // actorData.name = actor.replace(',100', '');

    // Make modifications to data here. For example:

    // Loop through ability scores, and add their modifiers to our sheet output.
    for (let [key, ability] of Object.entries(data.abilities)) {
      if (ability.value >= 0) {
        ability.modificateur = -5;
      }
      if (ability.value >= 2) {
        ability.modificateur = -4;
      }
      if (ability.value >= 4) {
        ability.modificateur = -3;
      }
      if (ability.value >= 6) {
        ability.modificateur = -2;
      }
      if (ability.value >= 8) {
        ability.modificateur = -1;
      }
      if (ability.value >= 10) {
        ability.modificateur = 0;
      }
      if (ability.value >= 12) {
        ability.modificateur = 1;
      }
      if (ability.value >= 14) {
        ability.modificateur = 2;
      }
      if (ability.value >= 16) {
        ability.modificateur = 3;
      }
      if (ability.value >= 18) {
        ability.modificateur = 4;
      }
      if (ability.value >= 20) {
        ability.modificateur = 5;
      }
    }
  }
}