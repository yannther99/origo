// Import Modules
import { origoActor } from "./actor/actor.js";
import { origoActorSheet } from "./actor/actor-sheet.js";
import { origoItem } from "./item/item.js";
import { origoItemSheet } from "./item/item-sheet.js";

Hooks.once('init', async function() {

  game.origo = {
    origoActor,
    origoItem,
    rollItemMacro
  };

  /**
   * Set an initiative formula for the system
   * @type {String}
   */
  CONFIG.Combat.initiative = {
    formula: "1d20",
    decimals: 2
  };

  // Define custom Entity classes
  CONFIG.Actor.entityClass = origoActor;
  CONFIG.Item.entityClass = origoItem;

  // Register sheet application classes
  Actors.unregisterSheet("core", ActorSheet);
  Actors.registerSheet("origo", origoActorSheet, { makeDefault: true });
  Items.unregisterSheet("core", ItemSheet);
  Items.registerSheet("origo", origoItemSheet, { makeDefault: true });

  // If you need to add Handlebars helpers, here are a few useful examples:
  Handlebars.registerHelper('concat', function() {
    var outStr = '';
    for (var arg in arguments) {
      if (typeof arguments[arg] != 'object') {
        outStr += arguments[arg];
      }
    }
    return outStr;
  });

  Handlebars.registerHelper('toLowerCase', function(str) {
    return str.toLowerCase();
  });

  Handlebars.registerHelper('isCritic', function(v1, v2) {
    if(v1 == 20 && v2 == "1d20") {
      return "<div class=\"roll-result greenCrit\">";
    } 
    else if(v1 == 1 && v2 == "1d20") {
      return "<div class=\"roll-result redCrit\">";
    } 
    else {
      return "<div class=\"roll-result\">";
    }
  });
});

Hooks.once("ready", async function() {
  // Wait to register hotbar drop hook on ready so that modules could register earlier if they want to
  Hooks.on("hotbarDrop", (bar, data, slot) => createBoilerplateMacro(data, slot));
});

/* -------------------------------------------- */
/*  Hotbar Macros                               */
/* -------------------------------------------- */

/**
 * Create a Macro from an Item drop.
 * Get an existing item macro if one exists, otherwise create a new one.
 * @param {Object} data     The dropped data
 * @param {number} slot     The hotbar slot to use
 * @returns {Promise}
 */
async function createBoilerplateMacro(data, slot) {
  if (data.type !== "Item") return;
  if (!("data" in data)) return ui.notifications.warn("You can only create macro buttons for owned Items");
  const item = data.data;

  // Create the macro command
  const command = `game.boilerplate.rollItemMacro("${item.name}");`;
  let macro = game.macros.entities.find(m => (m.name === item.name) && (m.command === command));
  if (!macro) {
    macro = await Macro.create({
      name: item.name,
      type: "script",
      img: item.img,
      command: command,
      flags: { "boilerplate.itemMacro": true }
    });
  }
  game.user.assignHotbarMacro(macro, slot);
  return false;
}

/**
 * Create a Macro from an Item drop.
 * Get an existing item macro if one exists, otherwise create a new one.
 * @param {string} itemName
 * @return {Promise}
 */
function rollItemMacro(itemName) {
  // TODO
  // const speaker = ChatMessage.getSpeaker();
  // let actor;
  // if (speaker.token) actor = game.actors.tokens[speaker.token];
  // if (!actor) actor = game.actors.get(speaker.actor);
  // const item = actor ? actor.items.find(i => i.name === itemName) : null;
  // if (!item) return ui.notifications.warn(`Your controlled Actor does not have an item named ${itemName}`);

  // // Trigger the item roll
  // return item.roll();
}

/**
 * Handle clickable rolls.
 * @param {Event} event   The originating click event
 * @private
 */
async function roll() {
  // TODO
  // // Basic template rendering data
  // const token = this.actor.token;
  // const item = this.data;
  // const actorData = this.actor ? this.actor.data.data : {};
  // const itemData = item.data;

  // // Define the roll formula.
  // let roll = new Roll('d20+@abilities.str.mod', actorData);
  // let label = `Rolling ${item.name}`;
  // // Roll and send to chat.
  // roll.roll().toMessage({
  //   speaker: ChatMessage.getSpeaker({ actor: this.actor }),
  //   flavor: label
  // });
}

Hooks.on("renderChatMessage", async (app, html, data) => {
  console.log(app);
  console.log(data);
  console.log(html);
  let hide=false;
  let messageId = app.data._id;
  let msg = game.messages.get(messageId);
  // let msgIndex = game.messages.entities.indexOf(msg);

  let _html = await html[0].outerHTML;
  let realuser = game.users.get(data.message.user);

  if(((data.cssClass == "whisper")||(data.message.type==1)) && game.user._id!=data.message.user && !game.user.isgM)
      hide=true;

  if(_html.includes("dice-roll") && !_html.includes("table-draw")){
      let rollData = {
          token:{
              img:"icons/svg/d20-black.svg",
              name:"Free Roll"
          },
          actor:realuser.data.name,
          flavor: app.data.flavor,
          formula: app.rolls[0]._formula,
          mod: 0,
          result: app.rolls[0]._total,
          dice: app.rolls[0]._dice,
          faces: app.rolls[0].dice[0].results[0].result,
          formulaS: app.rolls[0].dice[0].number + "d" + app.rolls[0].dice[0].faces,
          user: realuser.data.name
      };

      await renderTemplate("systems/origo/templates/dice.html", rollData).then(async newhtml => {

          let container = html[0];

          let content = html.find('.dice-roll');
          content.replaceWith(newhtml);

          _html = await html[0].outerHTML;


      });
  }

  if(!_html.includes("roll-template")){
      if(_html.includes("table-draw")){
          let mytableID = data.message.flags.core.RollTable;
          let mytable = game.tables.get(mytableID);
          //console.log(mytable.data.permission.default);
          if(mytable.data.permission.default==0)
              hide=true;
      }

      let containerDiv = document.createElement("DIV");

      let headerDiv = document.createElement("HEADER");
      let headertext = await fetch("systems/origo/templates/sbmessage.html").then(resp => resp.text());
      headerDiv.innerHTML = headertext;

      let msgcontent = html;
      let messageDiv = await document.createElement("DIV");
      messageDiv.innerHTML = _html;

      //containerDiv.appendChild(headerDiv);
      await containerDiv.appendChild(headerDiv);
      await containerDiv.appendChild(messageDiv);

      html = html[0].insertBefore(headerDiv,html[0].childNodes[0]);
      html = $(html);

  }

  if(!game.user.isGM && hide){
      //console.log(html);
      //console.log(_html);
      html.hide();
  }

  //ROLL INSTRUCTIONS
  let header = $(html).find(".message-header");
  header.remove();
  //console.log("tirando");
  let detail = await $(html).find(".roll-detail")[0];
  let result = $(html).find(".roll-result")[0];
  let clickdetail = $(html).find(".roll-detail-button")[0];
  let clickmain = $(html).find(".roll-main-button")[0];

  if(detail == null){
      return;
  }
  if(result==null){
      return;
  }
  if(clickdetail==null){
      return;
  }
  if(clickmain==null){
      return;
  }

  let detaildisplay = detail.style.display;
  detail.style.display = "none";
  let resultdisplay = result.style.display;
  let clickdetaildisplay = clickdetail.style.display;
  let clickmaindisplay = clickmain.style.display;
  clickmain.style.display = "none";

  $(html).find(".roll-detail-button").click(ev => {
      detail.style.display = detaildisplay;
      result.style.display = "none";
      $(html).find(".roll-detail-button").hide();
      $(html).find(".roll-main-button").show();
  });
  $(html).find(".roll-main-button").click(ev => {
      result.style.display = resultdisplay;
      detail.style.display = "none";
      $(html).find(".roll-detail-button").show();
      $(html).find(".roll-main-button").hide();
  });

  if(game.user.isGM){
      $(html).find(".roll-message-delete").click(async ev => {
          msg.delete(html);
      });
      auxMeth.rollToMenu();
  }
  else{
      if(game.user._id!=data.message.user)
          $(html).find(".roll-message-delete").hide();
  }
});