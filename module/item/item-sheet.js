/**
 * Extend the basic ItemSheet with some very simple modifications
 * @extends {ItemSheet}
 */
export class origoItemSheet extends ItemSheet {

  /** @override */
  static get defaultOptions() {
    return mergeObject(super.defaultOptions, {
      classes: ["origo", "sheet", "item"],
      width: 520,
      height: 480,
      tabs: [{ navSelector: ".sheet-tabs", contentSelector: ".sheet-body", initial: "description" }]
    });
  }

  /** @override */
  get template() {
    const path = "systems/origo/templates/item";
    // Return a single sheet for all item types.
    if (this.item.type == "armure") {
      return `${path}/item-armor.html`;
    }
    if (this.item.type == "arme à une main" || this.item.type == "arme à deux mains" || this.item.type == "arme" || this.item.type == "grande arme") {
      return `${path}/item-arme.html`;
    }
    else {
      return `${path}/item-sheet.html`;
    }
    
    // Alternatively, you could use the following return statement to do a
    // unique item sheet by type, like `weapon-sheet.html`.

    // return `${path}/${this.item.data.type}-sheet.html`;
  }

  /* -------------------------------------------- */

  /** @override */
  getData() {
    const data = super.getData();
    const itemData = data;
    if (itemData.item.type === 'arme à une main' || itemData.item.type === 'arme à deux mains') {
      this._prepareItem(data);
    }
    console.log(data);
    
    return data;
  }

  /* -------------------------------------------- */

  /** @override */
  setPosition(options = {}) {
    const position = super.setPosition(options);
    const sheetBody = this.element.find(".sheet-body");
    const bodyHeight = position.height - 192;
    sheetBody.css("height", bodyHeight);
    return position;
  }

  /* -------------------------------------------- */

  /** @override */
  activateListeners(html) {
    super.activateListeners(html);

    // Everything below here is only needed if the sheet is editable
    if (!this.options.editable) return;

    // Roll handlers, click handlers, etc. would go here.
    html.find('.poidsOption').click(ev => {
      const li = $(ev.currentTarget);
      console.log("test click");
      console.log(li);
    });
  }

  _prepareItem(sheetData) {
  //   const itemData = sheetData.data.stat;
  //   const poids = [{id:1,value:4,title:"Très léger",selected:-1},
  //                  {id:2,value:8,title:"Léger",selected:-1},
  //                  {id:3,value:10,title:"Moyen",selected:1},
  //                  {id:4,value:12,title:"Lourd",selected:-1},
  //                  {id:5,value:14,title:"Très lourd",selected:-1}];

  //   // Assign and return
  //   itemData.poids = poids;
  }
}
